import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import { map, Observable, Subscription } from 'rxjs';
import { IFlatNode } from './models/flat-node.model';
import { ITreeNode } from './models/tree-node.model';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  treeStructure!: ITreeNode[];

  subscription!: Subscription;

  private _transformer = (node: ITreeNode, level: number) => ({
    expandable: !!node.children && node.children.length > 0,
    name: node.name,
    level: level,
  });

  treeControl = new FlatTreeControl<IFlatNode>(
    (node) => node.level,
    (node) => node.expandable
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.children
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.subscription = this.dataService.getAllData().subscribe({
      next: (data) => (this.dataSource.data = data),
      error: (err) => console.log(err),
      complete: () => console.log('Data is fetched Successfully'),
    });
  }

  hasChild(_: number, node: IFlatNode) {
    return node.expandable;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
