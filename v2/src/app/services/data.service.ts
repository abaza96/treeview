import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ITreeNode } from '../models/tree-node.model';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private url = '/assets/characters.json';
  constructor(private http: HttpClient) {}

  getAllData(): Observable<ITreeNode[]> {
    return this.http.get<ITreeNode[]>(this.url).pipe(
      tap((data) => console.log('All', JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(handleError: HttpErrorResponse) {
    return throwError(
      () =>
        handleError.error instanceof ErrorEvent //Check the error
          ? `an Error Occured ${handleError.error.message}` // If error was a 400 Bad Request Error
          : `Server returned code: ${handleError.status}, Message: ${handleError.message}` // If error was a 500 Internal Server Error);
    );
  }
}
