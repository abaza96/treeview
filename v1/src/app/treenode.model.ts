export interface Tree {
  data?: any;
  children?: Tree[];
  leaf?: boolean;
  expanded?: boolean;
}
