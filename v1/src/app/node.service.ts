import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { Tree } from './treenode.model';
import { catchError, Observable, tap, throwError } from 'rxjs';
@Injectable()
export class NodeService {
  constructor(private http: HttpClient) {}
  getFilesystem() {
    return this.http
      .get('./filesystem.json')
      .pipe(
        tap(
          (data) => <TreeNode>JSON.stringify(data),
          catchError(this.handleError)
        )
      ) as Observable<Tree>;
  }

  private handleError(handleError: HttpErrorResponse) {
    let message =
      handleError.error instanceof ErrorEvent //Check the error
        ? `an Error Occured ${handleError.error.message}` // If error was a 400 Bad Request Error
        : `Server returned code: ${handleError.status}, Message: ${handleError.message}`; // If error was a 500 Internal Server Error
    console.error(message);
    return throwError(message);
  }
}
