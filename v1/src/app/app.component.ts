import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NodeService } from './node.service';
import { Tree } from './treenode.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  files!: Tree[];
  cols!: any[];
  subscription!: Subscription;
  constructor(private svc: NodeService) {}
  ngOnInit() {
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'size', header: 'Size' },
      { field: 'type', header: 'Type' },
    ];
    this.subscription = this.svc.getFilesystem().subscribe({
      next: (files) => this.files.push(files),
      error: (err) => console.log(err),
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
